import Vue from 'vue';
import csvtojson from './csvtojson';
var moment = require('moment');

new Vue({
  el: '#main-section',
  data: {
    key: '2PACX-1vQcAA3zuglOKFFlvWbu61rEPiJ-OKq38awqqJgXtNiX1R6v-AlhkRfDNuLEOKYn4xGhU2jQSu68DR9w',
    data: [],
    tab: 1,
  },
  computed: {
    types() {
      return this.data
        .map(item => item.type)
        .filter((val, index, arr) => arr.indexOf(val) == index)
    },
    data_filter() {
      var vm = this;
      return this.types.map(item => vm.data.filter(i => i.type == item));
    },

  },
  methods: {
    sorted(data) {

      var valid = JSON.parse(JSON.stringify(data));
      var invalid = JSON.parse(JSON.stringify(data));

      valid.reduce(function(item) {
        var dt = moment(item.expiry, 'Do MMMM YYYY').format('x');
        console.log(item.expiry);
        return true;
      })

      invalid.reduce(function(item) {
        var dt = moment(item.expiry, 'Do MMMM YYYY');
        return !dt.isValid();
      })

      console.log(valid, invalid);
      valid = valid.slice().sort(function(a, b) {
        var date1 = moment(a.expiry, 'Do MMMM YYYY')
        var date2 = moment(b.expiry, 'Do MMMM YYYY')
        return date2.format('X') - date1.format('X');
      });

      return valid.concat(invalid);

    },
    loadData: function(gid) {
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${this.key}/pub?gid=${gid}&single=true&output=csv`)
        .then(response => response.text())
        .then(csv => csvtojson(csv))
        .then(function(json) {
          vm.data = JSON.parse(json);

        });
    },
    dateformat(date) {
      return moment(date, 'DD/MM/YYYY').format("dddd, MMMM Do YYYY");
    }
  },
  mounted() {
    this.loadData(595081480);

  }
});