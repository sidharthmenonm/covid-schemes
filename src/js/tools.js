import Vue from 'vue';
import csvtojson from './csvtojson';
var moment = require('moment');

new Vue({
  el: '#main-section',
  data: {
    key: '2PACX-1vQcAA3zuglOKFFlvWbu61rEPiJ-OKq38awqqJgXtNiX1R6v-AlhkRfDNuLEOKYn4xGhU2jQSu68DR9w',
    data: [],
    tab: 1,
  },
  computed: {

  },
  methods: {
    loadData: function(gid) {
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${this.key}/pub?gid=${gid}&single=true&output=csv`)
        .then(response => response.text())
        .then(csv => csvtojson(csv))
        .then(function(json) {
          vm.data = JSON.parse(json);

        });
    },
    dateformat(date) {
      return moment(date, 'DD/MM/YYYY').format("dddd, MMMM Do YYYY");
    }
  },
  mounted() {
    this.loadData(690713300);

  }
});