import jQuery from "jquery";
import Vue from 'vue';
import csvtojson from './csvtojson';
var moment = require('moment');
import magnificPopup from 'magnific-popup';

new Vue({
  el: '#app',
  data: {
    key: '2PACX-1vQcAA3zuglOKFFlvWbu61rEPiJ-OKq38awqqJgXtNiX1R6v-AlhkRfDNuLEOKYn4xGhU2jQSu68DR9w',
    data: [],
    tab: 1,
  },
  computed: {
    upcoming() {
      return this.data.filter(function(item) {
        var fromdate = moment(item.date, 'DD/MM/YYYY')
        var today = moment();
        if (fromdate.isAfter(today)) {
          return true
        }
      })
    },
    previous() {
      return this.data.filter(function(item) {
        var fromdate = moment(item.date, 'DD/MM/YYYY')
        var today = moment();
        if (today.isAfter(fromdate)) {
          return true
        }
      })
    }
  },
  methods: {
    loadData: function(gid) {
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${this.key}/pub?gid=${gid}&single=true&output=csv`)
        .then(response => response.text())
        .then(csv => csvtojson(csv))
        .then(function(json) {
          vm.data = JSON.parse(json);
          console.log(json);
        });
    },
    dateformat(date) {
      return moment(date, 'DD/MM/YYYY').format("dddd, MMMM Do YYYY");
    }
  },
  mounted() {
    this.loadData(0);
    // 595081480
    jQuery('.video-pop-up').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false
    });
  }
});